package me.badstagram.vortex.core;


import me.badstagram.vortex.commandhandler.CommandClient;
import me.badstagram.vortex.commandhandler.CommandClientBuilder;
import me.badstagram.vortex.commands.admin.ErrorTest;
import me.badstagram.vortex.commands.info.PingCommand;
import me.badstagram.vortex.commands.info.UserInfo;
import me.badstagram.vortex.listeners.OnReadyListener;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.ChunkingFilter;
import net.dv8tion.jda.api.utils.cache.CacheFlag;
import net.explodingbush.ksoftapi.KSoftAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.security.auth.login.LoginException;
import java.util.Random;

public class Vortex {
    private static JDA jda;
    public static KSoftAPI kSoftAPI;

    private static Random rnd;
    private static final boolean devMode = true;
    private static final Logger log = LoggerFactory.getLogger(Vortex.class);


    public static void main(String[] args) {
        try {
            var client = new CommandClientBuilder()
                    .setPrefix(devMode ? "--" : "-")
                    .setOwnerId("424239181296959507")
                    .addCommand(new PingCommand())
                    .addCommand(new ErrorTest())
                    .addCommand(new UserInfo())
                    .build();

            jda = login(Config.get("token"), client);


        } catch (LoginException | InterruptedException e) {
            log.error("There was an error and Vortex was unable to start up!", e);
        }

    }

    protected static JDA login(@Nonnull String token, CommandClient client) throws LoginException, InterruptedException {
        return JDABuilder.createDefault(token)
                .disableCache(CacheFlag.ACTIVITY)
                .enableCache(CacheFlag.VOICE_STATE)
                .enableCache(CacheFlag.CLIENT_STATUS)
                .enableIntents(GatewayIntent.GUILD_MEMBERS)
                .enableIntents(GatewayIntent.GUILD_PRESENCES)
                .setChunkingFilter(ChunkingFilter.ALL)
                .setRawEventsEnabled(devMode)
                .setActivity(Activity.playing("Restarting..."))
                .setStatus(OnlineStatus.DO_NOT_DISTURB)
                .addEventListeners(client,
                        new OnReadyListener())
                .setBulkDeleteSplittingEnabled(false)
                .build()
                .awaitReady();
//
    }

    public static boolean isDevMode() {
        return devMode;
    }

    public static Random getRandom() {
        if (rnd == null) {
            rnd = new Random();
        }
        return rnd;
    }

    public static KSoftAPI getKSoftAPI() {
        if (kSoftAPI == null) {
            kSoftAPI = new KSoftAPI(Config.get("ksoft"));
        }
        return kSoftAPI;
    }

    public static JDA getJDA() {
        return jda;
    }
}
