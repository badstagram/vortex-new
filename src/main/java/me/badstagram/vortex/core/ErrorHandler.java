/*
 MIT License

 Copyright (c) 2020 badstagram

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

package me.badstagram.vortex.core;


import com.sun.istack.Nullable;
import me.badstagram.vortex.util.DatabaseUtils;
import me.badstagram.vortex.util.EmbedUtil;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.utils.MarkdownUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.UUID;
import java.util.stream.Collectors;

public class ErrorHandler {
    private ErrorHandler() {
    }

    /**
     * Handles any {@link Throwable} that may happen.
     *
     * @param thr   The Exception
     * @param event The event or {@code null}
     * @author badstagram
     */
    public static void handleException(Throwable thr, @Nullable GuildMessageReceivedEvent event) {

        if (Vortex.isDevMode()) {
            Logger logger = LoggerFactory.getLogger(ErrorHandler.class);
            logger.error(thr.getMessage(), thr); // print stacktrace to console;

            if (event != null) {
                event.getChannel().sendMessage(MarkdownUtil.codeblock(null, String.format("%s: %s", thr.getClass().getSimpleName(), thr.getMessage()))).queue();
            }
            return; // Disable sentry output on development
        }

        ResultSet rs = null;
        try (Connection con = DatabaseUtils.getConnection(); PreparedStatement ps = con.prepareStatement("INSERT INTO error_reporting (error_id, message_author, message_content, guild_id, exception_message, exception_stacktrace, exception_class) VALUES (?,?,?,?,?,?,?) RETURNING error_id")) {
            String stacktrace = Arrays.stream(thr.getStackTrace())
                    .map(StackTraceElement::toString)
                    .collect(Collectors.joining("\n"));



            ps.setString(1, UUID.randomUUID().toString());
            ps.setString(2, event == null ? "Unknown" : event.getAuthor().getId());
            ps.setString(3, event == null ? "Unknown" : event.getMessage().getContentRaw());
            ps.setString(4, event == null ? "Unknown" : event.getGuild().getId());
            ps.setString(5, thr.getMessage());
            ps.setString(6, stacktrace);
            ps.setString(7, thr.getClass().getName());


            rs = ps.executeQuery();

            if (event == null) return;

            if (rs.next()) {
                MessageEmbed embed = EmbedUtil.createDefault()
                        .setColor(Colors.ERROR.getAsColor())
                        .setTitle("An error occurred.")
                        .setDescription("Please report this in my " + MarkdownUtil.maskedLink("support server", Constants.SUPPORT_SERVER))
                        .setFooter("Using error ID " + rs.getString("error_id"))
                        .build();

                event.getChannel().sendMessage(embed).queue();
            }
        } catch (final Exception e) {
            Logger logger = LoggerFactory.getLogger(ErrorHandler.class);
            logger.error(e.getMessage(), e); // print stacktrace to console;

            MessageEmbed embed = EmbedUtil.createDefault()
                    .setColor(Colors.ERROR.getAsColor())
                    .setTitle("Uhh this is embarrassing")
                    .setDescription("An error occurred while reporting the previous error to my developer.")
                    .build();

            if (event != null) {
                event.getChannel().sendMessage(embed).queue();
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    protected String createGist(@Nonnull String text) {



        return "";
    }
}



