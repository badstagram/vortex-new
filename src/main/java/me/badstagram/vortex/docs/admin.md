---
description: Commands for administering Vortex.
---

# Admin

## eval

```yaml
Description:
- Run arbitrary Java code
Usage:
- eval <code>
Bot Permissions:
- Embed Links
User Permissions:
- Bot Owner
```


## gban

```yaml
Description:
- Report a global ban to the bot developer (or ban them if used by the developer)
Usage:
- gban <user id> <reason> <proof>
Bot Permissions:
- None
User Permissions:
- None
```

### gban remove


```yaml
Description:
- Remove the global ban for a user.
Usage:
- gban remove <user id> [reason]
Bot Permissions:
- Bot Owner
User Permissions:
- None
```

### gban approve 


```yaml
Description:
- Approve a global ban report.
Usage:
- gban approve <report id>
Bot Permissions:
- Bot Owner
User Permissions:
- None
```

### gban deny 


```yaml
Description:
- Deny a global ban report.
Usage:
- gban deny <report id> [reason]
Bot Permissions:
- Bot Owner
User Permissions:
- None
```
