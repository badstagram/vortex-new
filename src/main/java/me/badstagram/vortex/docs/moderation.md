---
description: Moderation commands.
---

# Moderation

## ban

```yaml
Description:
- Ban a user from the server.
- member can be one of, tag (Badstagram#0420), user id (424239181296959507) or mention (@Badstagram#0420)
Usage:
- ban <member> [reason]
- ban <user id> [reason]
Bot Permissions:
- Ban Members
User Permissions:
- Ban Members
```

##kick

```yaml
Description:
- Kick a user from the server.
- member can be one of, tag (Badstagram#0420), user id (424239181296959507) or mention (@Badstagram#0420)
Usage:
- kick <member> [reason]
Bot Permissions:
- Kick Members
User Permissions:
- Kick Members
```

##bulk ban

```yaml
Description:
- Ban multiple members from the server.
- member can be one of, tag (Badstagram#0420), user id (424239181296959507) or mention (@Badstagram#0420)
Usage:
- bulkban <members> [reason]
Bot Permissions:
- Ban Members
User Permissions:
- Ban Members
```

##Warn

```yaml
Description:
- Warn a user
Usage:
- warn <member>
Bot Permissions:
- None
User Permissions:
- Kick Members
```

##clear

```yaml
Description:
- Clear messages from a channel.
Usage:
- clear <amount>
Bot Permissions:
- Manage Messages
User Permissions:
- Manage Messages
```