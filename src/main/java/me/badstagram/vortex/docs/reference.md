### Command Prefixes
* Stable: `-`
* Nightly: `--`
* Development (not public): `vd!`
---
### Understanding the syntax
Every command begins with a prefix (which can be found above)

**Syntax**: `<prefix><command> [sub command] [argument1] [argument2]...`
* The arguments under `<argument>` are required arguments.
* The arguments under `[argument]` are optional arguments.

** Do not include `<>` or `[]` while making the commands.