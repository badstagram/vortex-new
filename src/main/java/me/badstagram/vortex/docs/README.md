[![Invite](https://img.shields.io/badge/Invite-Vortex-40e0d0?style=for-the-badge)](https://discordapp.com/oauth2/authorize?client_id=702129848910610452&scope=bot&permissions=8)
[![Discord](https://img.shields.io/discord/705938001519181877?label=Discord%20Server&logo=Discord&style=for-the-badge)](https://discord.gg/ZM9ftwE)


Vortex is a moderation, economy and fun bot made for [Discord](https://discord.com)

---
### Key features
* Auto Moderation system based on different actions.
* Discord User / Member profiles and much more.
* Automatically get money by talking in Text Channels.

---
### Getting Started
Use [this link](https://discordapp.com/oauth2/authorize?client_id=702129848910610452&scope=bot&permissions=8) to invite Vortex to your server

Or use [this link](https://discordapp.com/oauth2/authorize?client_id=702129848910610452&scope=bot&permissions=8) to invite Vortex Nightly to your server and get early access to new features

---
### The Community
For any kind of help and support regarding Vortex or anything else, you're welcome to [join our discord server](https://discord.gg/ZM9ftwE).

[![Discord Widget](https://discordapp.com/api/guilds/705938001519181877/widget.png?style=banner2)](https://discord.gg/7CrQEyP)

---
### Contributing
To contribute to the development of Cosmos bot, feel free to create a [new pull request on github.](https://gitlab.com/badstagram/vortex/-/merge_requests/new)

---
### Donate
Hosting Cosmos bot requires efficient and expensive servers which keeps Cosmos bot online with the least downtimes. 
Your support will encourage and help us a lot with the constant development process and power the servers to keep it up online all the time. 💛
* [Digital Ocean Referral Link ](https://m.do.co/c/cd4989e174ce) (Get a free $100, and we get $25 once you spend $25)
* Other options coming soon™

