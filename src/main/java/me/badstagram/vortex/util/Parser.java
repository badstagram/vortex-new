package me.badstagram.vortex.util;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.*;
import org.apache.commons.collections4.iterators.EnumerationIterator;
import org.jetbrains.annotations.Nullable;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {

    private final Message msg;
    private final JDA jda;
    private final Guild guild;
    private final String arg;
    private final Matcher matcher;


    public Parser(String arg, Message msg, JDA jda, Guild guild) {
        this.msg = msg;
        this.jda = jda;
        this.guild = guild;
        this.arg = arg;
        Pattern pattern = Pattern.compile("\"([^\"]*)\"");
        this.matcher = pattern.matcher(this.arg);
    }

    @Nullable
    public User parseUser() {
        User user;
        LoggerFactory.getLogger(this.getClass()).debug(this.arg);

        if (!this.msg.getMentionedMembers().isEmpty()) {
            return this.msg.getMentionedMembers().get(0).getUser();
        }

        String search = this.arg;

        LoggerFactory.getLogger(this.getClass()).debug("parseUser.search = {}", search);

        if (this.arg.matches("\\d+")) {
            user = this.jda.getUserById(search);
            LoggerFactory.getLogger(this.getClass()).debug("parseMember matches :)");
        } else {
            user = this.jda.getUsersByName(search, true)
                    .stream()
                    .findFirst()
                    .orElse(null);
        }

        if (user == null) {
            try {
                return this.jda.retrieveUserById(search).complete();
            } catch (Exception e) {
                return null;
            }
        }
        return user;
    }

    @Nullable
    public Member parseMember() {
        Member member;

        if (!this.msg.getMentionedMembers().isEmpty()) {
            return this.msg.getMentionedMembers().get(0);
        }

        String search;

        if (this.matcher.find()) {
            search = this.matcher.group(1);
        } else {
            search = arg;
        }

        LoggerFactory.getLogger(this.getClass()).debug("parseMember.search = {}", search);

        if (search.matches("\\d+")) {
            member = this.guild.getMemberById(search);
        } else {
            member = this.guild.getMembersByEffectiveName(search, true)
                    .stream()
                    .findFirst()
                    .orElse(null);
        }

        if (member == null) {
            try {
                return this.guild.retrieveMemberById(search).complete();
            } catch (Exception e) {
                return null;
            }
        }
        return member;
    }


    @Nullable
    public Role parseRole() {
        if (!this.msg.getMentionedRoles().isEmpty()) {
            return this.msg.getMentionedRoles().get(0);
        }

        Role role;
        String search;
        if (this.matcher.find()) {
            search = this.matcher.group(1);
        } else {
            search = arg;
        }
        if (search.matches("\\d+")) {
            role = this.guild.getRoleById(search);
        } else {
            role = this.guild.getRolesByName(search, true)
                    .stream()
                    .findFirst()
                    .orElse(null);
        }

        return role;
    }

    @Nullable
    public TextChannel parseChannel() {
        if (!this.msg.getMentionedMembers().isEmpty()) {
            return this.msg.getMentionedChannels().get(0);
        }

        TextChannel channel;
        String search;
        if (this.matcher.find()) {
            search = this.matcher.group(1);
        } else {
            search = arg;
        }
        if (search.matches("\\d+")) {
            channel = this.guild.getTextChannelById(search);
        } else {
            channel = this.guild.getTextChannelsByName(search, true)
                    .stream()
                    .findFirst()
                    .orElse(null);
        }

        return channel;
    }

    public static String parseUserFlags(@Nonnull User user) {

        StringBuilder sb = new StringBuilder();

        for (User.UserFlag flag : user.getFlags()) {
            switch (flag) {
                case BUG_HUNTER_LEVEL_1:
                    sb.append("<:bug_hunter:718646399297781790> ");
                    break;
                case BUG_HUNTER_LEVEL_2:
                    sb.append("<:bughuntergold:780917166274904114> ");
                    break;
                case EARLY_SUPPORTER:
                    sb.append("<:early_supporter:718646172549644298> ");
                    break;
                case HYPESQUAD:
                    sb.append("<:hypesquadevents:742397033461186690> ");
                    break;
                case HYPESQUAD_BALANCE:
                    sb.append("<:hypesquad_balance:718645777718575134> ");
                    break;
                case HYPESQUAD_BRAVERY:
                    sb.append("<:hypesquad_bravery:718645870622670921> ");
                    break;
                case HYPESQUAD_BRILLIANCE:
                    sb.append("<:hypesquad_brilliance:718646051422208010> ");
                    break;
                case PARTNER:
                    sb.append("<:partner:749386164523237459> ");
                    break;
                case STAFF:
                    sb.append("<:discord_staff:718646268917710918> ");
                    break;
                case SYSTEM:
                    sb.append("<:discord:749388843752358049> ");
                    break;
                case VERIFIED_BOT:
                case VERIFIED_DEVELOPER:
                    sb.append("<:verified:718646647005118485> ");
                    break;
                default:
                    break;

            }
        }

        // hacky workaround to see if a user has nitro
        if (user.getAvatarId() != null && user.getEffectiveAvatarUrl().endsWith(".gif") && user.getAvatarId().startsWith("a_")) {
            sb.append(" <:nitro:768055945280094208>");
        }

        if (user.isBot()) {
            sb.append(" <:bot_tag:762396082905940008>");
        }

        return sb.toString();
    }


}
