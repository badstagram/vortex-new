package me.badstagram.vortex.util;

import me.badstagram.vortex.core.Config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseUtils {
    private static Connection con = null;


    public static Connection getConnection() throws SQLException {
        if (con == null || con.isClosed()) {
            con = DriverManager.getConnection(Config.get("db_url"), Config.get("db_user"), Config.get("db_password"));
        }
        return con;
    }

}
