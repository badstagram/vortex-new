package me.badstagram.vortex.exceptions;

public class CommandExecutionException extends Exception{
    /**
     * Thrown when a command fails to be executed.
     * @param message The message.
     * @param cause The cause of the exception
     */
    public CommandExecutionException(String message, Throwable cause) {
        super(message, cause);
    }
}
