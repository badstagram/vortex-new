package me.badstagram.vortex.exceptions;


public class BadArgumentException extends Exception {
    /**
     * Thrown when an invalid command argument is provided.
     * @param message The message.
     */
    public BadArgumentException(String message) {
        super(message);
    }
}
