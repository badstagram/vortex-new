package me.badstagram.vortex.commandhandler;

import me.badstagram.vortex.core.Constants;
import me.badstagram.vortex.exceptions.BadArgumentException;
import me.badstagram.vortex.exceptions.CommandExecutionException;
import me.badstagram.vortex.util.EmbedUtil;
import me.badstagram.vortex.util.FormatUtil;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.stream.Collectors;

public abstract class Command {


    protected String name = "null";
    protected String help = "null";
    protected String usage = "null";
    protected boolean owner = false;
    protected int cooldown = 0;
    protected String[] aliases = new String[0];
    protected Permission[] botPermissions = Permission.EMPTY_PERMISSIONS;
    protected Permission[] userPermissions = Permission.EMPTY_PERMISSIONS;

    public abstract void execute(CommandContext ctx) throws CommandExecutionException, BadArgumentException;

    protected final void run(CommandContext ctx) {
        Message msg = ctx.getMessage();
        User usr = msg.getAuthor();
        Member member = msg.getMember();

        if (member == null || msg.isWebhookMessage()) {
            return;
        }

        if (!msg.isFromType(ChannelType.TEXT)) {
            return;
        }

        Member self = msg.getGuild().getSelfMember();

        if (!self.hasPermission(this.botPermissions)) {

            String perms = Arrays.stream(this.botPermissions)
                    .map(Permission::getName)
                    .collect(Collectors.joining());

            ctx.getChannel().sendMessageFormat(":x: I don't have the right permissions for this command. I need %s", perms).queue();
            return;
        }

        if (!member.hasPermission(this.userPermissions)) {
            String perms = Arrays.stream(this.userPermissions)
                    .map(Permission::getName)
                    .collect(Collectors.joining());

            ctx.getChannel().sendMessageFormat(":x: You don't have the right permissions for this command. You need %s", perms).queue();
            return;
        }

        if (!usr.getId().equals(ctx.getClient().getOwnerId()) && this.owner) {
            ctx.getChannel().sendMessage(":x: You don't have the right permissions for this command. You need Bot Owner").queue();
            return;
        }

        long expireTime = OffsetDateTime.now()
                .plusSeconds(this.cooldown)
                .toEpochSecond();

        final String key = String.format("U:%d|S:%d", usr.getIdLong(), ctx.getGuild().getIdLong());


        if (this.cooldown > 0) {
            int remaining = ctx.getClient().getRemainingCooldown(key);

            if (remaining > 0) {
                ctx.reply(String.format("This command is on cooldown for another %s", FormatUtil.secondsToTimeCompact(remaining)));
                return;
            } else ctx.getClient().applyCooldown(key, cooldown);
        }


        try {
            this.execute(ctx);

        } catch (CommandExecutionException e) {
            var embed = EmbedUtil.createDefaultError()
                    .setTitle("There was an unexpected error while executing that command")
                    .setDescription("If this error persists, report it in the [Support Server](%s)".formatted(Constants.SUPPORT_SERVER))
                    .setFooter(e.getCause().getClass().getCanonicalName())
                    .build();

            ctx.getChannel()
                    .sendMessage(embed)
                    .queue();
        } catch (BadArgumentException e) {
            var embed = EmbedUtil.createDefaultError()
                    .setTitle("One or more of the provided arguments is invalid or missing.")
                    .setDescription("Usage: %s. Refer to the [Docs](https://badstagram.gitbook.io/vortex) for more info.".formatted(this.usage))
                    .build();

            ctx.getChannel()
                    .sendMessage(embed)
                    .queue();
        }

    }

    public String getName() {
        return name;
    }

    public String getHelp() {
        return help;
    }

    public String getUsage() {
        return usage;
    }

    public boolean isOwner() {
        return owner;
    }

    public int getCooldown() {
        return cooldown;
    }

    public String[] getAliases() {
        return aliases;
    }

    public Permission[] getBotPermissions() {
        return botPermissions;
    }

    public Permission[] getUserPermissions() {
        return userPermissions;
    }
}
