FROM openjdk:15

COPY build/libs/ /usr/vortex

WORKDIR /usr/vortex


CMD ["java", "-jar", "./Vortex-1.0-all.jar"]
